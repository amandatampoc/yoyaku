const express = require('express')
const router = express.Router()
const Book = require("../models/book")
const Author = require("../models/author")
const book = require('../models/book')
// define which image types are accepted
const imageMimeTypes = ['image/jpeg', 'image/png', 'images/gif']


// All Books Route
router.get('/', async (req, res)=> {
    let query = Book.find()
    if (req.query.title != null && req.query.title != '') {
        query = query.regex('title' , new RegExp(req.query.title, 'i'))
    }
    if (req.query.publishedBefore != null && req.query.publishedBefore != '') {
        // lte = less than or equal
        query = query.lte('publishDate', req.query.publishedBefore)
      }
    if (req.query.publishedAfter != null && req.query.publishedAfter != '') {
        // gte = greater than or equal
        query = query.gte('publishDate', req.query.publishedAfter)
    }
    try {
        // await query defined above
        const books = await query.exec()
        res.render('books/index', {
            books:books,
            searchOptions: req.query
        })
    } catch {
        res.redirect('/')
    }
  
})

// New Book Route
router.get('/new', async (req, res) => {
   
    renderNewPage(res, new Book())
})

// Create Book Route
router.post('/',  async (req, res) => {
    const fileName = req.file != null ? req.file.filename : null
    const book = new Book({
        title: req.body.title,
        author: req.body.author,
        // converts 'publishDate' into a date format since it is passed on as a astring
        publishDate: new Date(req.body.publishDate),
        pageCount: req.body.pageCount,
        description: req.body.description
    })

    // call function for saving book cover image
    saveCover(book, req.body.cover)

    try {
        const newBook = await book.save()
        res.redirect(`books/${newBook.id}`)
    } catch {
        
        renderNewPage(res, book, true)
    }
})

// Show Book Router
router.get('/:id', async (req, res) => {
    try {
        // populate author variable inside book object
        const book = await Book.findById(req.params.id)
                                      .populate('author')
                                      .exec()
        res.render('books/show', { book: book })
    } catch {
        res.redirect('/')
    }
})

// Edit Book Route
router.get('/:id/edit', async (req, res) => {
    try {
      const book = await Book.findById(req.params.id)
      renderEditPage(res, book)
    } catch {
      res.redirect('/')
    }
  })


// Update Book Route
router.put('/:id',  async (req, res) => {
    let book

    try {
        book = await Book.findById(req.params.id)
        book.title = req.body.title
        book.author = req.body.author
        book.publishDate = new Date(req.body.publishDate)
        book.pageCount = req.body.pageCount
        book.description = req.body.description
        if (req.body.cover != null && req.body.cover !== ''){
            saveCover(book, req.body.cover)
        }
        await book.save()
        res.redirect(`/books/${book.id}`)
    } catch (err){
        console.log(err)
        if (book != null){
            renderEditPage(res, book, true)
        } else {
            redirect('/')
        }
    }
})

// Delete Book Page
router.delete('/:id', async (req, res) => {
    let book 
    try {
        book = await Book.findById(req.params.id)
        await book.remove()
        res.redirect('/books')
    } catch {
        if (book!=null){
            res.render('books/show', {
            book:book,
            errorMessage: 'Could not remove book' 
            }) 
        } else {
            res.redirect('/')
        }
    }
})

async function renderNewPage(res, book, hasError = false) {
    renderFormPage(res, book, 'new', hasError)
  }

async function renderEditPage(res, book, hasError = false) {
renderFormPage(res, book, 'edit', hasError)
}

  async function renderFormPage(res, book, form, hasError = false) {
    try {
      const authors = await Author.find({})
      const params = {
        authors: authors,
        book: book
      }
      if (hasError) {
        if (form === 'edit') {
          params.errorMessage = 'Error Updating Book'
        } else {
          params.errorMessage = 'Error Creating Book'
        }
      }
      res.render(`books/${form}`, params)
    } catch {
      res.redirect('/books')
    }
  }

// check if cover is a valid cover, if valid, save to book
function saveCover(book, coverEncoded) {
    if (coverEncoded == null) return
    // parse the string as JSON
    const cover = JSON.parse(coverEncoded)
    // if the cover is not null and includes the correct mime type, SAVE THE BOOK
    if (cover != null && imageMimeTypes.includes(cover.type)) {
        // convert buffer 
        book.coverImage = new Buffer.from(cover.data, 'base64')
        // extract buffer and convert image into correct type
        book.coverImageType = cover.type
        } 
}
module.exports = router