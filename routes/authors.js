const express = require('express')
const router = express.Router()
const Author = require("../models/author")
const Book = require('../models/book')

// All Authours Route
// use req.query because a get request sends a information to the query, post request sends request to the body
// check if there is a name passed on to the server to get regex case insensitive
router.get('/', async (req, res)=> {
  let searchOptions ={}
  if (req.query.name != null && req.query.name !==''){
    searchOptions.name = new RegExp(req.query.name, 'i')
  }
  try {
    const authors = await Author.find(searchOptions)
    res.render('authors/index', {
      authors: authors, 
      searchOptions: req.query
    })
  } catch {
    res.redirect('/')
  }
})

// New Author Route
router.get('/new', (req, res) => {
	res.render('authors/new', { author: new Author() })
  })

// Create Author Route
// req.body = body of form sent to the server
// name = name inputs from the form
// define name so that it will not send other information other than name 
router.post('/', async (req, res) => {
    const author = new Author({
      name:req.body.name
    })
  try {
    const newAuthor = await author.save()
      res.redirect(`authors/${newAuthor.id}`)
  } catch {
    res.render('authors/new', {
      author: author,
      errorMessage: 'Error creating Author'
    })
  }

})

// Show Author
// define this after new author because if you define new after this, it will think that /new is an id
// get id to be passed on request
router.get('/:id', async (req, res) =>{
  try {
    const author = await Author.findById(req.params.id)
    const books = await Book.find({ author: author.id }).limit(6).exec()
    res.render('authors/show', {
      author: author,
      booksByAuthor: books
    })
  } catch {

    res.redirect('/')
  }
})

// Edit Author
router.get('/:id/edit', async (req, res) => {
  try { 
    const author = await Author.findById(req.params.id)
    res.render('authors/edit', { author: author })
  } catch {
    res.redirect('/authors')
  }
})

// NOTE: In the browser, you can only use the get and post request, that is why we will install a new library called method-override so that we can use put and delete

// Update Author
router.put('/:id', async (req, res) => {
  let author
  try {
    const author = await Author.findById(req.params.id)
    author.name = req.body.name
    await author.save()
      res.redirect(`/authors/${author.id}`)
  } catch {
    if ( author == null ) {
      res.redirect('/')
    } else {
      res.render('authors/edit', {
        author: author,
        errorMessage: 'Error updating Author'
      })
    }
  }
  
})

// Delete Author
// used delete instead of get because google goes to every link of your page and it will wipe your data
router.delete('/:id', async (req, res) => {
  let author
  try {
    author = await Author.findById(req.params.id)
    await author.remove()
      res.redirect('/authors')
  } catch {
    if ( author == null ) {
      res.redirect('/')
    } else {
      res.redirect(`/authors/${author.id}`)
    }
  }
})
module.exports = router