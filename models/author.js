const mongoose = require('mongoose')
const Book = require('./book')

// define schema for Author
const authorSchema = new mongoose.Schema({
    name:{
        type: String,
        required: true
    }
})

// define foreign key to prevent delete if there is an associated book
authorSchema.pre('remove', function(next){
    Book.find({ author: this.id }, (err, books) =>{
    //    if there is an error, pass on the err message to the next function
        if (err) {
            next(err)
        } else if (books.length > 0) {
            next(new Error('This author has books still'))
        } else {
            next()
        }
    })
})

// export Schema. Author will be the name of the table in the database
module.exports = mongoose.model('Author', authorSchema)