const mongoose = require('mongoose')
const path = require('path')


// define schema for Author
const bookSchema = new mongoose.Schema({
    title: {
      type: String,
      required: true
    },
    description: {
      type: String
    },
    publishDate: {
      type: Date,
      required: true
    },
    pageCount: {
      type: Number,
      required: true
    },
    // define created at for the recent added feature
    createdAt: {
      type: Date,
      required: true,
      default: Date.now
    },
    // for passing image name only, image is on the server
    //no longer a string, defined as a buffer representing the image
    coverImage: {
      type: Buffer,
      required: true
    },
    // declare image type as string
    coverImageType:{
      type: String,
      required: true
    },
    // reference to the object id of authors inside the collection mongoose.Schema.Types.ObjectId
    author: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'Author'
    }
  })

// this property is not accessible in arrow function
bookSchema.virtual('coverImagePath').get(function() {
  if (this.coverImage != null && this.coverImageType != null) {
    // data object allow to take buffer data to use a source of image
    return `data:${this.coverImageType};charset=utf-8;base64,${this.coverImage.toString('base64')}`
  }
})

// export Schema. Book will be the name of the table in the database
module.exports = mongoose.model('Book', bookSchema)
