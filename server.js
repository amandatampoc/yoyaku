  
//check if it is running in production
if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config()
  }

  const express = require('express')
  const app = express()
  const expressLayouts = require('express-ejs-layouts')
  const bodyParser = require('body-parser')
  const methodOverride = require('method-override')

  // define routes
  const indexRouter = require('./routes/index')
  const authorRouter = require('./routes/authors')
  const bookRouter = require('./routes/books')

// set view engine
app.set('view engine', 'ejs')
app.set('views', __dirname + '/views')

app.set('layout', 'layouts/layout')
app.use(expressLayouts)
// to send a put and delete method via post
app.use(methodOverride('_method'))
app.use(express.static('public'))
// sending values via URL
app.use(bodyParser.urlencoded({ limit: '10mb', extended: false }))
 
// connection to mongoDB Atlas
const mongoose = require('mongoose')
const { urlencoded } = require('express')
mongoose.connect(process.env.DATABASE_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
 })
 const db = mongoose.connection
 db.on('error', error => console.error(error))
 db.once('open', () => console.log('Connected to Mongoose'))


// use routes that was defined
app.use('/', indexRouter)
app.use('/authors', authorRouter)
app.use('/books', bookRouter)

app.listen(process.env.PORT || 3000)