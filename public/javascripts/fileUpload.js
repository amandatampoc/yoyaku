// Plugins can be registered with FilePond using the registerPlugin method
FilePond.registerPlugin(
    FilePondPluginImagePreview,
    FilePondPluginImageResize,
    FilePondPluginFileEncode
)

// parse filepond
FilePond.parse(document.body)